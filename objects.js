// const person = {
//   firstName: "Jan",
//   lastName: "Kowalski",
//   job: {
//     position: "Software Developer",
//     mainLanguage: "Not JavaScript",
//   },
//   describeJob: function () {
//     console.log("this inside describeJob()", this);
//     console.log(
//       `${this.firstName} has job ${this.job.position} and main language is ${this.job.mainLanguage}`
//     );
//   },
//   welcome: function () {
//     console.log("this inside describeJob()", this);
//     console.log("hi " + this.firstName);
//   },
// };

// person.describeJob();
// person.welcome();

// const person = new Object(); // to samo co '{}'
// person.firstName = "Jan";
// person.lastName = "Kowalski";

// delete person.lastName;

// console.log(person);

// function Person(firstName, lastName, age) {
//   this.firstName = firstName;
//   this.lastName = lastName;
//   this.age = age;
//   this.describe = function () {
//     console.log(`${this.firstName} ${this.lastName} has age ${this.age}`);
//   };
// }

// const person = new Person("Jan", "Kowalski", 7);
// person.describe();

class Person {
  constructor(firstName, lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.allGrades = [];
  }

  get fullName() {
    return `${this.firstName} ${this.lastName}`;
  }

  set grades(grade) {
    this.allGrades.push(grade);
  }

  getWelcomeMessage = () => `Hello ${this.firstName}`;
}

const person = new Person("Arek", "Aptewicz");
console.log(person);
