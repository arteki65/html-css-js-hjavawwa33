const activeNavDstClass = "active-nav-dst";
const inactiveNavDstClass = "inactive-nav-dst";
const activeNavLinkClass = "active-nav-link";
const inactiveNavLinkClass = "inactive-nav-link";
const fakeTimeout = 500;

const navLinks = document.querySelectorAll("nav a");
navLinks.forEach((navLink) =>
  navLink.addEventListener("click", onNavLinkCliked)
);

function onNavLinkCliked(e) {
  const navDestination = e.srcElement.attributes.navdestination.value; // simple string (navDst1 or navDst2 or navDst3)

  markActiveNavLink(e);
  unmarkInactiveLinks(e, navDestination);

  hideContent(navDestination);

  showProgressBar();

  setTimeout(() => {
    hideProgressBar();
    showContent(navDestination);
  }, fakeTimeout);
}

function markActiveNavLink(e) {
  e.srcElement.classList.add(activeNavLinkClass);
  e.srcElement.classList.remove(inactiveNavLinkClass);
}

function unmarkInactiveLinks(e, navDestination) {
  document
    .querySelectorAll(`nav a:not([navdestination=${navDestination}])`)
    .forEach((e) => {
      e.classList.add(inactiveNavLinkClass);
      e.classList.remove(activeNavLinkClass);
    });
}

function hideContent(navDestination) {
  document
    .querySelectorAll(`main > div:not(#${navDestination})`)
    .forEach((el) => {
      el.classList.add(inactiveNavDstClass);
      el.classList.remove(activeNavDstClass);
    });
}

function showContent(navDestination) {
  const navDstInMain = document.querySelector("#" + navDestination); // np. #navDst1
  navDstInMain.classList.add(activeNavDstClass);
  navDstInMain.classList.remove(inactiveNavDstClass);
}

function showProgressBar() {
  const progressBar = document.querySelector("#infinite-progress-bar");
  progressBar.classList.add("visible");
  progressBar.classList.remove("hidden");
}

function hideProgressBar() {
  const progressBar = document.querySelector("#infinite-progress-bar");
  progressBar.classList.remove("visible");
  progressBar.classList.add("hidden");
}

let topVal = 0;
let intervalRunning = false;
let interval;

function startInterval() {
  intervalRunning = true;
  return setInterval(() => {
    const square = document.querySelector("#square");
    topVal = topVal + 5;
    square.style.top = topVal + "px";
  }, 500);
}

document.querySelector("#toogle-interval").addEventListener("click", () => {
  if (intervalRunning) {
    clearInterval(interval);
    intervalRunning = false;
  } else {
    interval = startInterval();
  }
});
