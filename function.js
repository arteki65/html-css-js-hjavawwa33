function add(a, b) {
  console.log(arguments);
  if (typeof a === "number" && typeof b === "number") {
    return a + b;
  }
  throw "this function should be called with numbers as arguments";
}

let funAdd = (a, b) => a + b;
funAdd(1, 1);

console.log(`result of add -> ${add("1", 8, 1, 2, 5)}`);
