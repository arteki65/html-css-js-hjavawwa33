const seven = "7";

function scopeDemo() {
  const seven = 7;
  let hiMessage = "Witamy w SDA";
  console.log(`Value id ${seven}`);
  if (seven % 2 === 1) {
    console.log(`Value ${seven} is odd`);
  }

  if (hiMessage.length > 5) {
    console.log(`string \'${hiMessage}\' has length greater than 5`);
  }
}

console.log(seven);
